package com.david.mongodb.dao;

import com.david.mongodb.model.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryProducto extends MongoRepository<Producto, String> {
}
