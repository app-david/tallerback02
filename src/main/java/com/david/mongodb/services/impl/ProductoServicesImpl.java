package com.david.mongodb.services.impl;

import com.david.mongodb.dao.RepositoryProducto;
import com.david.mongodb.model.Producto;
import com.david.mongodb.services.ProductoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoServicesImpl implements ProductoServices {

    @Autowired
    private RepositoryProducto repository;

    @Override
    public Producto createProducto(Producto producto) {
        return this.repository.insert(producto);
    }

    @Override
    public List<Producto> listProductos() {
        return this.repository.findAll();
    }

    @Override
    public Producto getProducto(String id) {
        Optional<Producto> result = this.repository.findById(id);
        return result.isPresent() ? result.get() : null;
    }

    @Override
    public Producto updateProducto(Producto producto) {
        return this.repository.save(producto);
    }

    @Override
    public void deleteProducto(String id) {
        this.repository.deleteById(id);
    }
}
