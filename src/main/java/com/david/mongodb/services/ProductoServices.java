package com.david.mongodb.services;

import com.david.mongodb.model.Producto;

import java.util.List;
import java.util.Optional;

public interface ProductoServices {

    Producto createProducto(Producto producto);
    List<Producto> listProductos();
    Producto getProducto(String id);
    Producto updateProducto(Producto producto);
    void deleteProducto(String id);

}
