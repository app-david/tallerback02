package com.david.mongodb.controller;

import com.david.mongodb.model.Producto;
import com.david.mongodb.services.ProductoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/productos")
public class UsuarioController {

    @Autowired
    private ProductoServices productoServices;

    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity listUsuarios(@PathVariable String idProducto){
        Producto producto = this.productoServices.getProducto(idProducto);
        if(producto == null) {
            return new ResponseEntity<>("No existe producto.", HttpStatus.NOT_FOUND);
        }
        if (producto.getUsuarios() != null && !producto.getUsuarios().isEmpty())
            return ResponseEntity.ok(producto.getUsuarios());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
