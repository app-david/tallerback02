package com.david.mongodb.controller;

import com.david.mongodb.model.Producto;
import com.david.mongodb.services.ProductoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/productos")
public class ProductoController {

    @Autowired
    private ProductoServices productoServices;

    @GetMapping
    public ResponseEntity listProductos(){
        List<Producto> list = this.productoServices.listProductos();
        if(list == null || list.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } else{
            return new ResponseEntity<>(list, HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Producto> getProducto(@PathVariable String id){
        Producto producto = this.productoServices.getProducto(id);
        if(producto != null)
            return ResponseEntity.ok(producto);
        else
            return new ResponseEntity("Producto no encontrado.", HttpStatus.NOT_FOUND);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Producto createProducto(@RequestBody Producto producto){
        return this.productoServices.createProducto(producto);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateProducto(@PathVariable String id, @RequestBody Producto productToUpdate) {
        Producto pr = this.productoServices.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productToUpdate.setId(id);
        this.productoServices.updateProducto(productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProducto(@PathVariable String id){
        this.productoServices.deleteProducto(id);
    }
}
